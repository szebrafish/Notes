package com.zebra.notes;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.IOException;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;

    ListView listViewNotes;
    static ArrayAdapter<String> notesAdapter;

    static ArrayList<String> notesList;


    //Pops up the alert dialog asking to confirm note deletion.
    //Alert includes the name of the note, truncated with ... if it is too long
    public void showAlert(String noteText, final int position) {

        String truncText;

        if (noteText.length() > 50) {

            truncText = noteText.substring(0, 49) + "...";

        } else {

            truncText = noteText;
        }

        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_delete)
                .setTitle("Delete Note?")
                .setMessage("Delete note  \"" + truncText + "\"?")
                .setNegativeButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        notesList.remove(position);

                        try {
                            sharedPreferences.edit().putString("Notes", ObjectSerializer.serialize(MainActivity.notesList)).apply();

                        } catch (IOException e) {
                            e.printStackTrace();

                        }

                        MainActivity.notesAdapter.notifyDataSetChanged();
                    }
                })
                .setPositiveButton("Cancel", null)
                .show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.addNote:

                Intent intent = new Intent(getApplicationContext(), NoteActivity.class);
                notesList.add("");
                intent.putExtra("Position", (notesList.size() - 1));
                startActivity(intent);

                return true;

            default:
                return false;

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharedPreferences = getSharedPreferences("com.zebra.notes", Context.MODE_PRIVATE);

        listViewNotes = (ListView) findViewById(R.id.listViewNotes);

        try {
            notesList = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString("Notes", ObjectSerializer.serialize(new ArrayList<String>())));

        } catch (IOException e) {
            e.printStackTrace();

        }

        if (notesList.size() == 0) {

            notesList.add("Example note");

        }

        //Created an updated Simple List Item layout which limits each ListView row to 2 lines
        notesAdapter = new ArrayAdapter<String>(this, R.layout.simple_list_item_1__2_lines_max_per_row, notesList);
        listViewNotes.setAdapter(notesAdapter);

        listViewNotes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(getApplicationContext(), NoteActivity.class);

                intent.putExtra("Position", position);

                startActivity(intent);

            }
        });

        //Creates alert on long-press, passes note text and ListView position to method
        listViewNotes.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                showAlert(notesList.get(position).toString(), position);
                
                return true;
            }
        });




    }
}
