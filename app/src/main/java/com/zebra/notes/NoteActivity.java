package com.zebra.notes;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

import java.io.IOException;

public class NoteActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;

    EditText editTextNote;

    int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);

        setTitle("Edit Note");

        editTextNote = (EditText) findViewById(R.id.editTextNote);

        sharedPreferences = getSharedPreferences("com.zebra.notes", Context.MODE_PRIVATE);

        Intent intent = getIntent();

        position = intent.getIntExtra("Position", 0);

        //"Append" trick below pushes cursor to end of text
        editTextNote.setText("");
        editTextNote.append(MainActivity.notesList.get(position));
    }


    @Override
    public void onBackPressed() {

        MainActivity.notesList.set(position, editTextNote.getText().toString());

        try {
            sharedPreferences.edit().putString("Notes", ObjectSerializer.serialize(MainActivity.notesList)).apply();

        } catch (IOException e) {
            e.printStackTrace();

        }

        MainActivity.notesAdapter.notifyDataSetChanged();
        finish();
    }
}
